import React from "react";
import { NavLink } from "react-router-dom";
import style from "./Header.module.css";
import { BsFillCartFill } from "react-icons/bs";
import { useSelector } from "react-redux";

export default function Header() {
  const navLinkClass = ({ isActive }) => {
    return isActive ? `nav-link ${style.activeNavItem}` : "nav-link";
  };
  const cart = useSelector((state) => state.cart);
  const quantity_cart = () => {
    return cart.reduce((total, item) => (total += item.quantity), 0);
  };
  return (
    <nav
      className={`${style.navbar} navbar-expand-sm navbar-dark bg-light mx-auto px-4 py-2`}
    >
      <button
        className=" d-lg-none"
        type="button"
        data-toggle="collapse"
        data-target="#collapsibleNavId"
        aria-controls="collapsibleNavId"
        aria-expanded="false"
        aria-label="Toggle navigation"
      />
      <div
        className="collapse navbar-collapse text-center row "
        id="collapsibleNavId"
      >
        <ul className="navbar-nav mt-2 mt-lg-0 col-4">
          <li className="nav-item mr-3">
            <NavLink
              style={{ color: "#4398ed" }}
              className={navLinkClass}
              to="/"
            >
              Home{" "}
            </NavLink>
          </li>
          <li className="nav-item mr-3">
            <NavLink
              style={{ color: "#4398ed" }}
              className={navLinkClass}
              to="/product"
            >
              Products
            </NavLink>
          </li>
          <li className="nav-item mr-3">
            <NavLink
              style={{ color: "#4398ed" }}
              className={navLinkClass}
              to="/review"
            >
              Reviews
            </NavLink>
          </li>
        </ul>
        <div
          style={{ fontSize: 30 }}
          className="text-primary text-center font-weight-bold col-4"
        >
          Beauty Shop
        </div>
        <NavLink
          to="/checkout"
          className="bg-transparent border-0 col-4 position-relative"
        >
          <BsFillCartFill className="text-primary" style={{ fontSize: 30 }} />
          <span className={style.cart_quantity}>{quantity_cart()}</span>
        </NavLink>
      </div>
    </nav>
  );
}
