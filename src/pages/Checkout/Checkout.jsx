import React from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  addTocart,
  deleteCart,
  checkOutCart,
} from "../../redux/ducks/cartSlice";
import style from "./CheckoutStyle.module.css";
import { AiOutlineMinus, AiFillDelete } from "react-icons/ai";
import { BsPlus } from "react-icons/bs";
import Header from "../../components/Header/Header";
import toast, { Toaster } from "react-hot-toast";
import { useNavigate } from "react-router-dom";
export default function Checkout() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const cartList = useSelector((state) => state.cart);
  const handleIncrease = (product) => {
    const foughtIndex = cartList.findIndex(
      (item) => item.productId === product.productId
    );
    let cartListClone = [...cartList];
    cartListClone[foughtIndex] = {
      ...cartListClone[foughtIndex],
      quantity: product.quantity + 1,
    };
    console.log(cartListClone, "clonecart");
    dispatch(addTocart(cartListClone[foughtIndex]));
  };

  const handleDecrease = (product) => {
    const foughtIndex = cartList.findIndex(
      (item) => item.productId === product.productId
    );
    let cartListClone = [...cartList];
    if (product.quantity === 1) {
      dispatch(deleteCart(foughtIndex));
    } else {
      cartListClone[foughtIndex] = {
        ...cartListClone[foughtIndex],
        quantity: product.quantity - 1,
      };
      console.log(cartListClone, "clonecart");
      dispatch(addTocart(cartListClone[foughtIndex]));
    }
  };
  const caclSubTotal = () => {
    if (cartList.legth === 0) return 0;
    return Math.round(
      cartList.reduce((total, item) => total + item.quantity * item.price, 0)
    );
  };
  const caclShipCost = () => {
    if (!cartList.length) return 0;

    return 10;
  };
  return (
    <>
      <Header />
      <div className="container">
        <h2 style={{ marginBottom: 60 }} className="text-center text-danger">
          My Shopping Cart
        </h2>
        <div className="cart_content d-flex mr-0 justify-content-between">
          <div className="cart_list w-50">
            {!cartList.length ? (
              <div><img className="d-block w-100 h-100" src="https://rtworkspace.com/wp-content/plugins/rtworkspace-ecommerce-wp-plugin/assets/img/empty-cart.png"/></div>
            ) : (
              cartList.map((item, index) => (
                <div key={index} className={style.cart_item}>
                  <img src={item.imageUrl} alt="img_product" />
                  <div
                    onClick={() => {
                      dispatch(deleteCart(index));
                    }}
                    className={`${style.icon_delete}`}
                  >
                    <AiFillDelete style={{ fill: "red", fontSize: 30 }} />
                  </div>
                  <div className="detail">
                    <p className="display-5 font-weight-bold">
                      {item.productName}
                    </p>
                    <p>{item.description}</p>
                    <div className="d-flex">
                      <div className="quantity d-flex">
                        <button
                          onClick={() => handleDecrease(item)}
                          className="btn-warning btn font-weight-bold d-flex justify-content-center align-items-center"
                        >
                          <AiOutlineMinus />
                        </button>
                        <span className="d-flex justify-content-center align-items-center font-weight-bold px-3">
                          {item.quantity}
                        </span>
                        <button
                          onClick={() => {
                            handleIncrease(item);
                          }}
                          className="btn-primary btn font-weight-bold d-flex justify-content-center align-items-center"
                        >
                          <BsPlus />
                        </button>
                      </div>
                      <span className="price ml-5">
                        ${Math.round(item.quantity * item.price)}
                      </span>
                    </div>
                  </div>
                </div>
              ))
            )}
          </div>
          <div className="order">
            <div className={`${style.order_info}`}>
              <h3 className=" mb-3 text-danger font-weight-bold">Order Info</h3>
              <p>
                <span className="mr-5">Subtotal</span>
                <span className="font-weight-bold">${caclSubTotal()}</span>
              </p>
              <p>
                <span className="mr-5">Shipping cost</span>
                <span className="font-weight-bold">
                  ${!cartList.length ? 0 : 10}
                </span>
              </p>
              <p style={{ fontSize: 30 }} className="font-weight-bold">
                <span className="mr-5">Total</span>
                <span>${caclSubTotal() + caclShipCost()}</span>
              </p>
            </div>
            <div className="checkout mt-4">
              <button
                disabled={!cartList.length ? true : false}
                onClick={() => {
                  alert("Do you want to purchase?");
                  toast("Thank you for purchased !", {
                    icon: "👏",
                  });
                  dispatch(checkOutCart());
                  navigate("/product");
                }}
                className="d-block w-100 btn btn-primary rounded"
              >
                Checkout
              </button>
              <Toaster />

              <button
                onClick={() => {
                  navigate("/product");
                }}
                className="d-block w-100 btn btn-light rounded mt-2 btn-outline-primary"
              >
                Continue Shopping
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
