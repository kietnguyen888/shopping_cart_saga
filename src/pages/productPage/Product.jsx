import React, { useEffect, useState } from "react";
import toast, { Toaster } from "react-hot-toast";
import { useDispatch, useSelector } from "react-redux";
import Header from "../../components/Header/Header";
import { addTocart } from "../../redux/ducks/cartSlice";
import { getProduct } from "../../redux/ducks/productSlice";
import style from "./ProductStyle.module.css";
import {AiOutlineMinus} from  "react-icons/ai";
import {BsPlus} from "react-icons/bs"
export default function Product() {
  const dispatch = useDispatch();

  
  const productList = useSelector((state) => state.product);

  const [detailProduct, setDetailProduct] = useState(null);
  const renderDetail = () => {
    if (!detailProduct) {
      setDetailProduct({ ...productList[0], quantity: 1 });
    }

    const handleDecrease = () => {
      if (detailProduct.quantity === 1) return;
      else
        setDetailProduct({
          ...detailProduct,
          quantity: detailProduct.quantity - 1,
        });
    };
    const handleIncrease = () => {
      setDetailProduct({
        ...detailProduct,
        quantity: detailProduct.quantity + 1,
      });
    };
    return (
      <div className={style.product_detail}>
        <div className="info">
          <div className="img_product">
            <img src={detailProduct?.imageUrl} alt="img" />
          </div>
          <p style={{fontSize:40}} className=" font-weight-bold">
            {detailProduct?.productName}
          </p>
          <p>{detailProduct?.description}</p>
        </div>
        <div className="action">
          <div className={style.quantity}>
            <button
              onClick={handleDecrease}
              className="btn-warning btn font-weight-bold d-flex justify-content-center align-items-center"
            >
              <AiOutlineMinus />
            </button>
            <span className="d-flex justify-content-center align-items-center font-weight-bold px-3">
              {detailProduct?.quantity}
            </span>
            <button
              onClick={handleIncrease}
              className="btn-primary btn font-weight-bold d-flex justify-content-center align-items-center"
            >
              <BsPlus />
            </button>
          </div>
          <div style={{fontSize:30}} className="price">
            ${(detailProduct?.quantity * detailProduct?.price).toFixed(2)}
          </div>
          <button
            onClick={() => {
              toast.success("Add to cart successfully!");
              dispatch(addTocart(detailProduct));
            }}
            className="btn btn-success"
          >
            Add to cart
          </button>
          <Toaster />
        </div>
      </div>
    );
  };
  return (
    <div >
      <Header />
      {productList.length > 0 ? (
        <div className="body d-flex p-5 container-fluid" style={{height:"90vh"}}>
          {renderDetail()}
          <div className={style.product_list}>
            {productList.map((item, index) => (
              <div className={`${style.content_item} d-flex`} key={index}>
                <img
                  style={{ width: "20%", height: "65%" }}
                  src={item.imageUrl}
                  alt="img_product"
                />
                <div className="detail">
                  <p className="display-5 font-weight-bold">
                    {item.productName}
                  </p>
                  <p>{item.description}</p>
                  <p>
                    <span className="display-5 font-weight-bold">
                      ${item.price}
                    </span>
                    <button
                      className="btn btn-info ml-5"
                      onClick={() => {
                        setDetailProduct({ ...item, quantity: 1 });
                      }}
                    >
                      Detail
                    </button>
                  </p>
                </div>
              </div>
            ))}
          </div>
        </div>
      ) : (
        ""
      )}
    </div>
  );
}
