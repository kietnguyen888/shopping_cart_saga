import React from "react";
import Header from "../../components/Header/Header";

export default function Review() {
  return (
    <>
      {" "}
      <Header />
      <div>
        <div className="d-flex w-100 justify-content-center align-items-center">
          <img
            className="w-25"
            src="https://res.cloudinary.com/dyotzt92h/image/upload/v1632934092/Fsoft-academy/Pngtree_coming_soon_notification_transparen_background_6484862_vjudel.png"
            alt
          />
        </div>
      </div>
    </>
  );
}
