import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { BrowserRouter, Route, Routes, useNavigate } from "react-router-dom";
import Loading from "./components/Loading";
import Checkout from "./pages/Checkout/Checkout";
import Home from "./pages/HomePage/Home";
import Product from "./pages/productPage/Product";
import Review from "./pages/Review/Review";
import { getProduct } from "./redux/ducks/productSlice";

export default function App() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const product = useSelector((state) => state.product);
  console.log(product);
  useEffect(() => {
    dispatch(getProduct());
  }, [dispatch]);
  useEffect(() => {
    navigate("/product");
  }, []);
  return (
    <div className="App">
      {product.length === 0 ? (
        <Loading />
      ) : (
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/product" element={<Product />} />
          <Route path="/review" element={<Review />} />
          <Route path="/checkout" element={<Checkout />} />
        </Routes>
      )}
    </div>
  );
}
