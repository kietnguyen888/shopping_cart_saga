import {
  configureStore,
  combineReducers,
  getDefaultMiddleware,
} from "@reduxjs/toolkit";
import createSagaMiddleware from "redux-saga";
import { watcherSaga } from "./sagas/rootSaga";
import productReducer from "./ducks/productSlice";
import cartReducer from "./ducks/cartSlice";

const sagaMiddleware = createSagaMiddleware();

const reducer = combineReducers({
  product: productReducer,
  cart: cartReducer,
});

const store = configureStore({
  reducer,
  middleware: [...getDefaultMiddleware({ thunk: false }), sagaMiddleware],
});
sagaMiddleware.run(watcherSaga);

export default store;
