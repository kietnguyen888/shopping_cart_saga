import axios from "axios";

export function requestGetProduct() {
  return axios.request({
    method: "get",
    url: "https://fa-asm-redux-backend.herokuapp.com/api/products"
  });
}
