import { createSlice } from "@reduxjs/toolkit";

const productSlice = createSlice({
  name: "product",
  initialState: [],
  reducers: {
    getProduct() {},
    setProduct(state, action) {
      const productData = action.payload;

      return productData ;
    },
  },
});

export const { getProduct, setProduct } = productSlice.actions;

export default productSlice.reducer;
